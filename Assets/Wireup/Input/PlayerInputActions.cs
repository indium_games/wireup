// GENERATED AUTOMATICALLY FROM 'Assets/Wireup/Input/PlayerInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Default"",
            ""id"": ""7c0811a1-c1aa-466a-9e64-311337774906"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""463f5cd5-10fe-4a31-bd1f-93c5fc6845e8"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Aim"",
                    ""type"": ""PassThrough"",
                    ""id"": ""920406fb-e0b3-4d78-9822-3b624dc37f87"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShootIn"",
                    ""type"": ""Button"",
                    ""id"": ""fc21bcb8-47d2-48a0-9908-7d92b960c469"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShootOut"",
                    ""type"": ""Button"",
                    ""id"": ""bbc1bd23-e393-4c70-968c-4c302bfad312"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cut"",
                    ""type"": ""Button"",
                    ""id"": ""8a46ee63-8387-4a60-bd6a-7c2dbee052a4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""98cf0976-9126-4489-8cf4-11ee1af10b21"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ControlsMenu"",
                    ""type"": ""Button"",
                    ""id"": ""413febd6-b974-41b5-8f8a-10aa73fc025f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""3b00c285-23f9-4923-a900-52fb7fe4dde6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""db00ab38-9ba5-4f6a-a4f6-d28224cf924f"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""480f84a1-2808-43e1-aea7-35e77ece7592"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""ad08e101-91b6-45ba-9ef7-f4e6d76cd9d7"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""5383e83a-ac6d-48cd-a5fa-76d3c13a0e30"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""98bd7a83-44da-4f40-9ab1-3fc8a937ece9"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b5bfc42-1f53-44f1-bb25-69d4639d71ed"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShootIn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7800ffde-25c3-4702-bddd-b1f211989045"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShootOut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8367824c-9837-46a2-9884-7d9ab85e4eb6"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4a1d846b-a298-4523-a692-f89201f4b064"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""adf0b5af-efd1-41e1-ab71-081377093f98"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ControlsMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c5b6d0d-d2ae-4c92-bda5-8891bb47aafe"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ControlsMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Default
        m_Default = asset.FindActionMap("Default", throwIfNotFound: true);
        m_Default_Move = m_Default.FindAction("Move", throwIfNotFound: true);
        m_Default_Aim = m_Default.FindAction("Aim", throwIfNotFound: true);
        m_Default_ShootIn = m_Default.FindAction("ShootIn", throwIfNotFound: true);
        m_Default_ShootOut = m_Default.FindAction("ShootOut", throwIfNotFound: true);
        m_Default_Cut = m_Default.FindAction("Cut", throwIfNotFound: true);
        m_Default_Run = m_Default.FindAction("Run", throwIfNotFound: true);
        m_Default_ControlsMenu = m_Default.FindAction("ControlsMenu", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Default
    private readonly InputActionMap m_Default;
    private IDefaultActions m_DefaultActionsCallbackInterface;
    private readonly InputAction m_Default_Move;
    private readonly InputAction m_Default_Aim;
    private readonly InputAction m_Default_ShootIn;
    private readonly InputAction m_Default_ShootOut;
    private readonly InputAction m_Default_Cut;
    private readonly InputAction m_Default_Run;
    private readonly InputAction m_Default_ControlsMenu;
    public struct DefaultActions
    {
        private @PlayerInputActions m_Wrapper;
        public DefaultActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Default_Move;
        public InputAction @Aim => m_Wrapper.m_Default_Aim;
        public InputAction @ShootIn => m_Wrapper.m_Default_ShootIn;
        public InputAction @ShootOut => m_Wrapper.m_Default_ShootOut;
        public InputAction @Cut => m_Wrapper.m_Default_Cut;
        public InputAction @Run => m_Wrapper.m_Default_Run;
        public InputAction @ControlsMenu => m_Wrapper.m_Default_ControlsMenu;
        public InputActionMap Get() { return m_Wrapper.m_Default; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DefaultActions set) { return set.Get(); }
        public void SetCallbacks(IDefaultActions instance)
        {
            if (m_Wrapper.m_DefaultActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMove;
                @Aim.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnAim;
                @Aim.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnAim;
                @Aim.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnAim;
                @ShootIn.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootIn;
                @ShootIn.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootIn;
                @ShootIn.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootIn;
                @ShootOut.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootOut;
                @ShootOut.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootOut;
                @ShootOut.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnShootOut;
                @Cut.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnCut;
                @Cut.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnCut;
                @Cut.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnCut;
                @Run.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnRun;
                @Run.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnRun;
                @Run.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnRun;
                @ControlsMenu.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnControlsMenu;
                @ControlsMenu.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnControlsMenu;
                @ControlsMenu.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnControlsMenu;
            }
            m_Wrapper.m_DefaultActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Aim.started += instance.OnAim;
                @Aim.performed += instance.OnAim;
                @Aim.canceled += instance.OnAim;
                @ShootIn.started += instance.OnShootIn;
                @ShootIn.performed += instance.OnShootIn;
                @ShootIn.canceled += instance.OnShootIn;
                @ShootOut.started += instance.OnShootOut;
                @ShootOut.performed += instance.OnShootOut;
                @ShootOut.canceled += instance.OnShootOut;
                @Cut.started += instance.OnCut;
                @Cut.performed += instance.OnCut;
                @Cut.canceled += instance.OnCut;
                @Run.started += instance.OnRun;
                @Run.performed += instance.OnRun;
                @Run.canceled += instance.OnRun;
                @ControlsMenu.started += instance.OnControlsMenu;
                @ControlsMenu.performed += instance.OnControlsMenu;
                @ControlsMenu.canceled += instance.OnControlsMenu;
            }
        }
    }
    public DefaultActions @Default => new DefaultActions(this);
    public interface IDefaultActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnAim(InputAction.CallbackContext context);
        void OnShootIn(InputAction.CallbackContext context);
        void OnShootOut(InputAction.CallbackContext context);
        void OnCut(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
        void OnControlsMenu(InputAction.CallbackContext context);
    }
}
