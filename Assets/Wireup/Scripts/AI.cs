﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public enum RequestType {In, Out }

public class AI : MonoBehaviour
{
    public static AI Instance;
    public event Action InRequest;
    public event Action GameOver;
    public List<Terminal> Terminals;
    List<Terminal> activeTerminals = new List<Terminal>();
    List<Terminal> activeInputs = new List<Terminal>();
    List<Terminal> activeOutputs = new List<Terminal>();
    public int ConnectionsCount;

    // Start is called before the first frame update
    private void Awake()
    {
        Cursor.visible = false;
        if (Instance == null)
            Instance = this;
    }
    void Start()
    {
        Terminals = GetComponentsInChildren<Terminal>().ToList();
        
        foreach(Terminal t in Terminals)
        {
            void addInSocket() { activeTerminals.Add(t); activeInputs.Add(t); };
            void removeInSocket() { activeTerminals.Remove(t); activeInputs.Remove(t); };
            void addOutSocket() { activeTerminals.Add(t); activeOutputs.Add(t); };
            void removeOutSocket() { activeTerminals.Remove(t); activeOutputs.Remove(t); };

            t.InSocket.SwitchedOn += addInSocket;
            t.InSocket.SwitchedOff += removeInSocket;
            t.OutSocket.SwitchedOn += addOutSocket;
            t.OutSocket.SwitchedOff += removeOutSocket;

            void death()
            {
                t.InSocket.SwitchedOn -= addInSocket;
                t.InSocket.SwitchedOff -= removeInSocket;
                t.OutSocket.SwitchedOn -= addOutSocket;
                t.OutSocket.SwitchedOff -= removeOutSocket;
                Terminals.Remove(t);
                t.Dead -= death;
                
            };
            t.Dead += death;

        }
        Terminals[0].InSocket.SwitchOn();
        Terminals[1].OutSocket.SwitchOn();
    }

    // Update is called once per frame
    float timer = 10f;
    int requestAmount = 1;

    bool gameover;
    private void Update()
    {


        // Terminal[] inputs = Terminals.Where(_ => !(_.Input.Active) && _.Output.Connected).ToArray();
        // Terminal[] outputs = Terminals.Where(_ => !(_.Output.Active) || (_.Input.Active && _.Input.Connected)).ToArray();

        timer -= Time.deltaTime;

        // if(activeInputs.Count = 0 && activeOutputs.)
        if (Terminals.Count <= 1 && !gameover)
        {
            gameover = true;
            GameOver?.Invoke();
        }

        if (timer < 0)
        {
            for (int index = 0; index < requestAmount; index++)
            {
                List<Terminal> common = Terminals.Except(activeTerminals).ToList();
                if (common.Count > 0)
                {
                    Terminal t = common[UnityEngine.Random.Range(0, common.Count)];

                    t.InSocket.SwitchOn();
                    common.Remove(t);
                    if (common.Count > 0)
                    {
                        t = common[UnityEngine.Random.Range(0, common.Count)];
                        t.OutSocket.SwitchOn();
                        common.Remove(t);
                    }
                }
                timer = 10f;
                if((ConnectionsCount > requestAmount + 3) && (requestAmount <= Terminals.Count/2))
                {
                    requestAmount++;
                }
            }
        }
    }

    public void RestoreOut()
    {
        if (activeOutputs.Count == 0)
        {
            List<Terminal> common = Terminals.Except(activeTerminals).ToList();
            Terminal t = common[UnityEngine.Random.Range(0, common.Count)];
            t.OutSocket.SwitchOn();
        }
    }

    public void RestoreIn()
    {
        if (activeInputs.Count == 0)
        {
            List<Terminal> common = Terminals.Except(activeTerminals).ToList();
            Terminal t = common[UnityEngine.Random.Range(0, common.Count)];
            t.InSocket.SwitchOn();
        }
    }

}
