﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[System.Serializable]
public class Plug
{
    public LayerMask Mask => mask;
    [SerializeField] private LayerMask mask;

    [HideInInspector]
    public bool InUse => !go.activeSelf;
    
    [SerializeField] private GameObject go;

    public Transform Slot => slot;
    [SerializeField] private Transform slot;

    public void Switch(bool on)
    {
        go.SetActive(on);
    }
}

public class Shooter : MonoBehaviour
{
    [SerializeField] private Camera eye;
    [SerializeField] private float maxDist;

    [SerializeField] private Transform cutPivot;
    [SerializeField] private Vector3 cutExtents;
    [SerializeField] private Animator cutterAnimator;

    [SerializeField] private Animator gun;
    [SerializeField] private GameObject segmentPrefab;

    [SerializeField] private Material[] wireMaterialsDim;
    [SerializeField] private Material[] wireMaterialsBright;

    [SerializeField] private Plug inPlug;
    [SerializeField] private Plug outPlug;    

    private int gunLayer;
    private int defaultLayer;
    private RaycastHit shotHit;

    private Wire currentWire;
    public List<Wire> Wires { get; private set; } = new List<Wire>();

    private void Start()
    {
        Player.Instance.InputActions.Default.ShootIn.performed += ShootIn_performed;
        Player.Instance.InputActions.Default.ShootOut.performed += ShootOut_performed;
        Player.Instance.InputActions.Default.Cut.performed += Cut_performed;

        gunLayer = LayerMask.NameToLayer("Gun");
        defaultLayer = LayerMask.NameToLayer("Default");

        Wire.SegmentPrefab = segmentPrefab;
    }

    private Collider[] cutBuffer = new Collider[5];

    private void Cut_performed(InputAction.CallbackContext ctx)
    {
        cutterAnimator.SetTrigger("Cut");
        int count = Physics.OverlapBoxNonAlloc(cutPivot.position, cutExtents, cutBuffer, eye.transform.rotation, LayerMask.GetMask("Wire"));

        if (count > 0)
            for (int i = 0; i < count; i++)
                Destroy(cutBuffer[i].transform.parent.parent.gameObject);
    }

    private void ShootIn_performed(InputAction.CallbackContext ctx)
    {
        Socket socket = ShootPlug(inPlug);

        if (inPlug.InUse)
        {
            ProcessShot(socket);
        }
        else if (currentWire != null && currentWire.FirstSocket.IsInSocket)
        {
            currentWire.Kill();
            currentWire = null;

            return;
        }

        gun.SetTrigger("LShot");
    }

    private void ShootOut_performed(InputAction.CallbackContext ctx)
    {
         Socket socket = ShootPlug(outPlug);

        if (outPlug.InUse)
        {
            ProcessShot(socket);
        }
        else if (currentWire != null && !currentWire.FirstSocket.IsInSocket)
        {
            currentWire.Kill();
            currentWire = null;

            return;
        }

        gun.SetTrigger("RShot");
    }

    private void ProcessShot(Socket socket)
    {
        if (currentWire != null && currentWire.State == WireState.CONSTRUCTING)
            CloseWire(socket);
        else
        {
            int matId = Random.Range(0, wireMaterialsBright.Length);
            currentWire = new Wire(socket, GetHangPos(socket.WireJunction.position.y), wireMaterialsBright[matId], wireMaterialsDim[matId]);
            currentWire.DestroyedUnderConstruction += CurrentWire_DestroyedUnderConstruction;
        }
    }

    private void CurrentWire_DestroyedUnderConstruction()
    {
        if (inPlug.InUse)
            ShootPlug(inPlug);
        if (outPlug.InUse)
            ShootPlug(outPlug);
    }

    private Vector3 GetHangPos(float y)
    {
        Vector3 pos = Player.Instance.transform.position + Player.Instance.transform.forward;
        pos.y = y;
        return pos;
    }

    private void CloseWire(Socket socket)
    {
        currentWire.Finish(socket, GetHangPos(socket.WireJunction.position.y));
        ShootPlug(inPlug);
        ShootPlug(outPlug);
        Wires.Add(currentWire);
        currentWire.Guard.Destroyed += Wire_Destroyed;
        currentWire = null;
    }

    private void Wire_Destroyed(Wire wire)
    {
        if (wire == currentWire)
            currentWire = null;
        Wires.Remove(wire);
    }

    private void Update()
    {
        if (currentWire != null && currentWire.State == WireState.CONSTRUCTING)
        {
            Vector3 pos = Player.Instance.transform.position;
            pos.y = currentWire.Points[currentWire.Points.Count - 1].y;

            currentWire.ResolvePoints(pos);

            Vector3 hePos = inPlug.InUse ? inPlug.Slot.position : outPlug.Slot.position;
            currentWire.UpdateHungingEnd(hePos);
        }
    }

    private Socket ShootPlug(Plug plug)
    {
        if (!plug.InUse)
        {
            Ray ray = eye.ScreenPointToRay(new Vector3(eye.pixelWidth/2, eye.pixelHeight/2, 0));
            if (Physics.Raycast(ray, out shotHit, maxDist, LayerMask.GetMask("ShotBlock", "InSocket", "OutSocket")))
            {
                if ((plug.Mask & 1 << shotHit.collider.gameObject.layer) == 0)
                    return null;

                Socket socket = shotHit.collider.transform.GetComponent<Socket>();
                if ((socket.State & (SocketState.PLUGGED | SocketState.ON_CONNECTED)) != 0)
                    return null;

                plug.Switch(false);
                return socket;
            }
        }
        else
        {
            plug.Switch(true);
        }

        return null;
    }

    private void OnDrawGizmos()
    {
        Ray ray = eye.ScreenPointToRay(new Vector3(eye.pixelWidth / 2, eye.pixelHeight / 2, 0));
        Gizmos.DrawLine(ray.origin, ray.GetPoint(20f));

        if (currentWire != null && currentWire.State == WireState.CONSTRUCTING)
        {
            currentWire.DrawWire();
            if (inPlug.InUse)
            {
                Vector3 pos = Player.Instance.transform.position + Player.Instance.transform.forward;
                pos.y = currentWire.Points[currentWire.Points.Count - 1].y;

                Gizmos.DrawCube(pos, Vector3.one * .1f);
                Gizmos.DrawLine(currentWire.Points[currentWire.Points.Count-1], pos);
            }
        }
    }
}
