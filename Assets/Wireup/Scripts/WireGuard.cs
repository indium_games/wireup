﻿using UnityEngine;

public class WireGuard : MonoBehaviour
{
    public event System.Action<Wire> Destroyed;
    public Wire Wire;

    private void Update()
    {
        if (Wire != null)
        {
            switch (Wire.State)
            {
                case WireState.CONSTRUCTING:
                    Wire.UpdateFirstSegment();
                    break;
                case WireState.READY:
                case WireState.CONNECTION:
                    Wire.UpdateFirstSegment();
                    Wire.UpdateLastSegment();
                    break;
            }
        }
    }

    private void OnDestroy()
    {
        Destroyed?.Invoke(Wire);
    }
}
