﻿using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum WireState
{
    CONSTRUCTING = 1,
    READY = 2,
    CONNECTION = 4,
    DESTROYED = 8
}

public class Segmet
{
    public Segmet(GameObject go, MeshRenderer renderer)
    {
        GO = go;
        Renderer = renderer;
    }

    public readonly GameObject GO;
    public readonly MeshRenderer Renderer;
}

public class Wire
{
    public static GameObject SegmentPrefab;

    public event System.Action DestroyedUnderConstruction;

    public WireGuard Guard { get; private set; }
    public List<Vector3> Points { get; private set; } = new List<Vector3>();
    public List<Segmet> WireSegments { get; private set; } = new List<Segmet>();

    public Socket FirstSocket { get; }
    public Socket SecondSocket { get; private set; }

    private Connection connection;

    private GameObject parent;
    private GameObject hungingEnd;

    private RaycastHit edgeHit;
    private LayerMask mask;

    public WireState State
    {
        get => state;
        private set
        {
            WireState prev = state;
            state = value;

            if (prev == WireState.CONSTRUCTING && value == WireState.DESTROYED)
                DestroyedUnderConstruction?.Invoke();

            if (prev == WireState.CONNECTION && value == WireState.READY)
                DimMaterial();
            else if (prev == WireState.READY && value == WireState.CONNECTION)
                BrightMaterial();
        }
    }
    private WireState state;

    private Material materialBright;
    private Material materialDim;

    public Wire(Socket pFirst, Vector3 hangingEnd, Material pMaterialBright, Material pMaterialDim)
    {
        materialBright = pMaterialBright;
        materialDim = pMaterialDim;

        mask = LayerMask.GetMask("Obstacle");
        parent = new GameObject
        {
            name = "Wire"
        };

        Vector3 pos = pFirst.WireJunction.position;
        pos.y = 1.6f;

        Vector3 fwd = pFirst.WireJunction.forward;
        Vector3 up = Vector3.up;
        Vector3.OrthoNormalize(ref up, ref fwd);

        Points.Add(pos);
        Points.Add(pos + fwd * .45f);

        WireSegments.Add(GenerateSegment());
        hungingEnd = GenerateSegment(Points[1], hangingEnd).GO;
        hungingEnd.gameObject.layer = 0;
        hungingEnd.transform.GetChild(0).gameObject.layer = 0;

        FirstSocket = pFirst;
        FirstSocket.Destroyed += Kill;
        FirstSocket.Plug();

        State = WireState.CONSTRUCTING;

        Guard = parent.AddComponent<WireGuard>();
        Guard.Wire = this;
        Guard.Destroyed += (_) => CleanUp();
    }

    private float intensity = 1;

    private void DimMaterial()
    {
        foreach (Segmet seg in WireSegments)
            seg.Renderer.material = materialDim;
    }

    private void BrightMaterial()
    {
        foreach (Segmet seg in WireSegments)
            seg.Renderer.material = materialBright;
    }

    public void Finish(Socket pSecond, Vector3 hint, float step = .05f)
    {
        Vector3 pos = pSecond.WireJunction.position;
        pos.y = 1.6f;

        Vector3 fwd = pSecond.WireJunction.forward;
        Vector3 up = Vector3.up;
        Vector3.OrthoNormalize(ref up, ref fwd);

        Vector3 end = pos + fwd * .45f;

        float t = 0f;

        while (t < 1f)
        {
            ResolvePoints(Vector3.Lerp(hint, end, t));
            t += step;
        }

        Points.Add(end);
        WireSegments.Add(GenerateSegment());
        Points.Add(pos);
        WireSegments.Add(GenerateSegment());

        Object.Destroy(hungingEnd);

        SecondSocket = pSecond;
        SecondSocket.Destroyed += Kill;
        SecondSocket.Plug();

        State = WireState.READY;

        if ((FirstSocket.State & SocketState.ON) != 0 && (SecondSocket.State & SocketState.ON) != 0)
            Connect();

        FirstSocket.SwitchedOn += Socket_SwitchedOn;
        FirstSocket.SwitchedOff += Socket_SwitchedOff;
    }

    public void Kill()
    {
        Object.Destroy(parent);
    }

    public void ResolvePoints(Vector3 pos)
    {
        if (Physics.Linecast(pos, Points[Points.Count - 1], out edgeHit, mask))
        {
            Points.Add(edgeHit.point + edgeHit.normal * .1f);
            WireSegments.Add(GenerateSegment());
            return;
        }

        if (Points.Count > 2 &&
            !Physics.Linecast(pos, Points[Points.Count - 2], mask) &&
            !HasObstacles(Points[Points.Count - 1], pos, 2))
        {
            Points.RemoveAt(Points.Count - 1);
            Object.Destroy(WireSegments[WireSegments.Count - 1].GO);
            WireSegments.RemoveAt(WireSegments.Count - 1);
        }
    }

    private int activeSockets = 0;

    private void Socket_SwitchedOn()
    {
        ++activeSockets;
        if (activeSockets == 2)
            Connect();
    }

    private void Socket_SwitchedOff()
    {
        --activeSockets;
    }

    private void Connect()
    {
        connection = new Connection(FirstSocket, SecondSocket, Guard, Random.Range(5f,10f));
        connection.Expired += Connection_Expired;
        State = WireState.CONNECTION;
        AI.Instance.ConnectionsCount++;
    }

    private void Connection_Expired()
    {
        State = WireState.READY;
        activeSockets = 0;
    }

    public void UpdateHungingEnd(Vector3 pos)
    {
        hungingEnd.transform.position = Points[Points.Count - 1];
        Vector3 dir = pos - Points[Points.Count - 1];
        hungingEnd.transform.rotation = Quaternion.LookRotation(dir);
        hungingEnd.transform.localScale = new Vector3(1, 1, dir.magnitude);
    }

    public void UpdateFirstSegment()
    {
        Segmet seg = WireSegments[0];
        Vector3 start = FirstSocket.WireJunction.position;
        Vector3 end = Points[1];
        UpdateSegment(seg.GO, start, end);
    }

    public void UpdateLastSegment()
    {
        Segmet seg = WireSegments[WireSegments.Count - 1];
        Vector3 start = SecondSocket.WireJunction.position;
        Vector3 end = Points[Points.Count - 2];
        UpdateSegment(seg.GO, start, end);
    }

    private void UpdateSegment(GameObject go, Vector3 start, Vector3 end)
    {
        go.transform.position = start;
        Vector3 dir = end - start;
        go.transform.rotation = Quaternion.LookRotation(dir);
        go.transform.localScale = new Vector3(1, 1, dir.magnitude);
    }

    private Segmet GenerateSegment()
    {
        return GenerateSegment(Points[Points.Count - 2], Points[Points.Count - 1]);
    }

    private Segmet GenerateSegment(Vector3 start, Vector3 end)
    {
        GameObject go = Object.Instantiate(SegmentPrefab);
        MeshRenderer renderer = go.GetComponentInChildren<MeshRenderer>();
        renderer.material = materialDim;

        UpdateSegment(go, start, end);
        go.transform.parent = parent.transform;

        return new Segmet(go, renderer);
    }

    private bool HasObstacles(Vector3 start, Vector3 end, int depth)
    {
        if (depth == 0)
            return false;

        Vector3 middle = Vector3.Lerp(start, end, .5f);
        bool res = Physics.Linecast(Points[Points.Count - 2], middle, mask);
        if (res)
            return true;

        return HasObstacles(start, middle, depth - 1) || HasObstacles(middle, end, depth - 1);
    }

    private void CleanUp()
    {
        FirstSocket.Destroyed -= Kill;
        FirstSocket.Unplug();        

        if (SecondSocket == null)
            Object.Destroy(hungingEnd);
        else
        {
            FirstSocket.Destroyed -= Kill;
            SecondSocket.Unplug();            
        }

        Points.Clear();
        WireSegments.Clear();

        State = WireState.DESTROYED;
    }

    public void DrawWire()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < Points.Count; i++)
        {
            if (Points.Count > i + 1)
                Gizmos.DrawLine(Points[i], Points[i + 1]);
            Gizmos.DrawSphere(Points[i], .1f);
        }
    }
}