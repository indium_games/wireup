﻿using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; } 

    [SerializeField] private float speed = 5;
    [SerializeField] private float accel = 1.75f;

    [SerializeField] private float sensitivityX = 5;
    [SerializeField] private float sensitivityY = 3;
    [SerializeField] private Transform eye;

    [SerializeField] private GameObject controlsScreen;

    public PlayerInputActions InputActions { get; private set; }

    private Vector3 move;
    private Vector2 mouse;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            InputActions = new PlayerInputActions();

            controlsScreen.SetActive(false);
        }
        else
            Destroy(gameObject);
    }
    private void Start()
    {
        AI.Instance.GameOver += () => { InputActions.Disable(); };
    }
    private void OnEnable()
    {
        InputActions.Enable();
        InputActions.Default.Move.performed += Move_performed;
        InputActions.Default.Aim.performed += Aim_performed;
        InputActions.Default.Run.performed += Run_performed;
        InputActions.Default.Run.canceled += Run_canceled;
        InputActions.Default.ControlsMenu.performed += ControlsMenu_performed;
    }

    private void ControlsMenu_performed(InputAction.CallbackContext obj)
    {
        controlsScreen.SetActive(!controlsScreen.activeSelf);
    }

    private void Run_canceled(InputAction.CallbackContext ctx)
    {
        speed /= accel;
    }

    private void Run_performed(InputAction.CallbackContext ctx)
    {
        speed *= accel;
    }

    private float xRot;

    private void Aim_performed(InputAction.CallbackContext ctx)
    {
        mouse = ctx.ReadValue<Vector2>();
        xRot -= mouse.y * sensitivityY * Time.deltaTime;
        xRot = Mathf.Clamp(xRot, -75f, 75f);
    }

    private void Move_performed(InputAction.CallbackContext ctx)
    {
        Vector2 input = ctx.ReadValue<Vector2>();
        move.x = input.x;
        move.z = input.y;
    }

    private void FixedUpdate()
    {
        transform.position += transform.TransformDirection(move * speed * Time.deltaTime);
        transform.Rotate(Vector3.up, mouse.x * sensitivityX * Time.deltaTime);
        eye.localRotation = Quaternion.Euler(xRot, 0, 0);
    }

    private void OnDisable()
    {
        InputActions.Disable();
    }
}
