﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    // Start is called before the first frame update
    private void Awake()
    {

    }
    void Start()
    {
        AI.Instance.GameOver += () => {
            GetComponentInChildren<Button>().enabled = true;
            GetComponentInChildren<RawImage>().enabled = true;
            GetComponentInChildren<Exit>().enabled = true;
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
