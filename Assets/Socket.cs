﻿using System.Collections;
using UnityEngine;

public class Connection
{
    public event System.Action Expired;

    public Connection(Socket first, Socket second, WireGuard guard, float timer)
    {
        Player.Instance.StartCoroutine(Timer(timer));

        first.Connect(this);
        second.Connect(this);

        guard.Destroyed += Guard_Destroyed;
    }

    private void Guard_Destroyed(Wire wire)
    {
        
    }

    private IEnumerator Timer(float time)
    {
        yield return new WaitForSeconds(time);
        Expired?.Invoke();
    }
}

[System.Flags]
public enum SocketState
{
    OFF_UNPLUGGED = 1,
    OFF_PLUGGED =   2,
    ON_UNPLUGGED =  4,
    ON_PLUGGED =    8,
    ON_CONNECTED =  16,    
    DESTROYED =     32,

    ON = ON_PLUGGED | ON_UNPLUGGED | ON_CONNECTED,
    OFF = OFF_PLUGGED | OFF_UNPLUGGED,
    PLUGGED = ON_PLUGGED | OFF_PLUGGED,
    UNPLUGGED = ON_UNPLUGGED | OFF_UNPLUGGED
}

public class Socket : MonoBehaviour
{
    public event System.Action SwitchedOn;
    public event System.Action SwitchedOff;
    public event System.Action Connected;
    public event System.Action Interrupted;
    public event System.Action Destroyed;

    public Terminal Terminal { get; private set; }
    public bool IsInSocket { get; private set; } = false;

    [SerializeField] private GameObject fakePlug;

    public Transform WireJunction => wireJunction;
    [SerializeField] private Transform wireJunction;
    
    [SerializeField] private float activeTime = 10f;

    private Coroutine offTimer;

    public SocketState State
    {
        get => state;
        private set
        {
            SocketState prev = state;
            state = value;

            if (prev == SocketState.ON_CONNECTED && (value & SocketState.UNPLUGGED) != 0)
                Interrupted?.Invoke();

            if ((value & SocketState.ON) != 0 && (prev & SocketState.OFF) != 0)
                SwitchedOn?.Invoke();
            else if ((value & SocketState.OFF) != 0 && (prev & SocketState.ON) != 0)
                SwitchedOff?.Invoke();

            if (value == SocketState.ON_CONNECTED)
                Connected?.Invoke();
        }
    }
    private SocketState state = SocketState.OFF_UNPLUGGED;

    private void Awake()
    {
        Terminal = GetComponentInParent<Terminal>();
        if ((LayerMask.GetMask("InSocket") & 1 << gameObject.layer) != 0)
            IsInSocket = true;
    }


    public void Connect(Connection connection)
    {
        if (State == SocketState.ON_PLUGGED)
            State = SocketState.ON_CONNECTED;
        else
            throw new System.Exception($"Invalid FSM transition {State} -> {SocketState.ON_CONNECTED}");

        if (offTimer != null)
            StopCoroutine(offTimer);

        connection.Expired += SwitchOff;
    }

    public void Plug()
    {
        switch (State)
        {
            case SocketState.OFF_UNPLUGGED:
                State = SocketState.OFF_PLUGGED;
                break;
            case SocketState.ON_UNPLUGGED:
                State = SocketState.ON_PLUGGED;
                break;
            case SocketState.DESTROYED:
                return;
            default:
                throw new System.Exception($"Invalid FSM transition {State} -> {SocketState.PLUGGED}");
        }

        fakePlug.SetActive(true);
    }

    public void Unplug()
    {
        switch (State)
        {
            case SocketState.OFF_PLUGGED:
                State = SocketState.OFF_UNPLUGGED;
                break;
            case SocketState.ON_PLUGGED:
            case SocketState.ON_CONNECTED:
                State = SocketState.ON_UNPLUGGED;
                break;
            case SocketState.DESTROYED:
                return;
            default:
                throw new System.Exception($"Invalid FSM transition {State} -> {SocketState.UNPLUGGED}");
        }

        fakePlug.SetActive(false);
    }

    public void SwitchOn()
    {
        switch (State)
        {
            case SocketState.OFF_UNPLUGGED:
                State = SocketState.ON_UNPLUGGED;
                break;
            case SocketState.OFF_PLUGGED:
                State = SocketState.ON_PLUGGED;
                break;
            case SocketState.DESTROYED:
                return;
            default:
                throw new System.Exception($"Invalid FSM transition {State} -> {SocketState.ON}");
        }
        activeTime = Random.Range(10f, 20f);

        offTimer = StartCoroutine(Timer(activeTime));
    }

    private void SwitchOff()
    {
        switch (State)
        {
            case SocketState.ON_UNPLUGGED:
                State = SocketState.OFF_UNPLUGGED;
                break;
            case SocketState.ON_CONNECTED:
            case SocketState.ON_PLUGGED:
                State = SocketState.OFF_PLUGGED;
                break;
            case SocketState.DESTROYED:
                return;
            default:
                throw new System.Exception($"Invalid FSM transition {State} -> {SocketState.OFF}");
        }
    }

    private void OnDisable()
    {
        State = SocketState.DESTROYED;
        Destroyed?.Invoke();
    }

    private IEnumerator Timer(float time)
    {
        yield return new WaitForSeconds(time);
        SwitchOff();
    }
}
