﻿using System.Collections;
using System.Linq;
using System;
using UnityEngine;

public class Terminal : MonoBehaviour
{
    public Socket InSocket;
    public Socket OutSocket;

    public event Action Dead;

    private RequestType request;    

    [HideInInspector]
    public bool InUse { get; set; } = false;

    [SerializeField] private int MaxHP = 1000;
    private int HP;
    [SerializeField] private int HPDegen = 10;
    [SerializeField] private int HPRegen = 15;
    [SerializeField] private int InterruptDmg = 200; 
    
    private int activeSockets = 0;
    private int connectedSockets = 0;

    private Animator animator;
    private Collider _collider;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        animator = GetComponent<Animator>();

        InSocket.SwitchedOn += InSocket_SwitchedOn;
        OutSocket.SwitchedOn += OutSocket_SwitchedOn;

        InSocket.SwitchedOff += InSocket_SwitchedOff;
        OutSocket.SwitchedOff += OutSocket_SwitchedOff;

        InSocket.Interrupted += Socket_Interrupted;
        OutSocket.Interrupted += Socket_Interrupted;

        InSocket.Connected += Socket_Connected;
        OutSocket.Connected += Socket_Connected;

    }

    private void Start()
    {
        HP = MaxHP;
        StartCoroutine(HealthCare());
    }

    private IEnumerator HealthCare()
    {
        while (true)
        {
            if (connectedSockets > 0)
            {
                //Debug.Log($"HP regen {transform}");
                HP += HPRegen;
            }

            if (activeSockets - connectedSockets > 0)            
                HP -= HPDegen;

            HP = Mathf.Min(HP, MaxHP);

            float hpFraction = (float)HP / MaxHP;
            if (hpFraction < 0)
            {
                Debug.Log(hpFraction);
                animator.SetBool("Die",true);
                break;
            }

            animator.SetFloat("HPFraction", 1 - hpFraction);
            //Debug.Log($"Connected: {connectedSockets} Active: {activeSockets} HPFraction: {hpFraction}");
            yield return new WaitForSeconds(.2f);
        }
    }

    private void InSocket_SwitchedOn()
    {
        ++activeSockets;
        animator.SetBool("CallIn", true);
        InSocket.Destroyed += AI.Instance.RestoreIn;

    }
    private void OutSocket_SwitchedOn()
    {
        ++activeSockets;
        animator.SetBool("CallOut", true);
        OutSocket.Destroyed += AI.Instance.RestoreOut;
    }


    private void InSocket_SwitchedOff()
    {
        --activeSockets;
        if (connectedSockets > 0)
            --connectedSockets;

        if (activeSockets == 0)
        {
            animator.SetBool("CallIn", false);

            animator.SetBool("Connected", false);
        }
   
    }

    private void OutSocket_SwitchedOff()
    {
        --activeSockets;
        if (connectedSockets > 0)
            --connectedSockets;

        if (activeSockets == 0)
        {

            animator.SetBool("CallOut", false);
            animator.SetBool("Connected", false);
        }

    }


    private void Socket_Connected()
    {
        ++connectedSockets;
        animator.SetBool("Connected", true);
    }

    private void Socket_Interrupted()
    {
        animator.SetBool("Connected", false);
 
        Debug.Log("Interrupted ");
    }

    public void DeathAnim()
    {
        Dead?.Invoke();
        _collider.enabled = false;
        InSocket.gameObject.SetActive(false);
        OutSocket.gameObject.SetActive(false);

        InSocket.SwitchedOn -= InSocket_SwitchedOn;
        OutSocket.SwitchedOn -= InSocket_SwitchedOn;

        InSocket.SwitchedOff -= InSocket_SwitchedOff;
        OutSocket.SwitchedOff -= InSocket_SwitchedOff;

        InSocket.Interrupted -= Socket_Interrupted;
        OutSocket.Interrupted -= Socket_Interrupted;

        InSocket.Connected -= Socket_Connected;
        OutSocket.Connected -= Socket_Connected;

    }

}
